var fs = require('fs');
var pmongo = require('promised-mongo');
var co = require('co');


var db = pmongo('mongodb://localhost:27017/farfetch', ['items']);

var writeStreamForRu = fs.createWriteStream("ru.xls");
var writeStreamForDe = fs.createWriteStream("de.xls");

var header = "url" + "\t" + "name"+ "\t" + " thingID" + "\t" + "price" + "\t" +"size" +"\n";
writeStreamForRu.write(header);
writeStreamForDe.write(header);

var item = db.items.find();

co(function*(){

	yield item.forEach(function(el){

		var row = el.url + "\t" + el.name +"\t"+ el.thingID +"\t"+ el.price + "\t" + el.size +"\n";

		if (el.url.match(/https:\/\/www.farfetch.com\/ru\/.+/g)) {
		  writeStreamForRu.write(row);
		} else {
		  writeStreamForDe.write(row);
		}

	});
})
.then(function(){
	writeStreamForRu.close();
	writeStreamForDe.close();

	db.close();
})





