'use strict'

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var pmongo = require('promised-mongo');
var co = require('co');
var toProcess = new Map(), 
    inProcess = new Map();
var CronJob = require('cron').CronJob;

var routes = require('./routes/index')(toProcess, inProcess);
var db = pmongo('mongodb://localhost:27017/farfetch', ['urls']);
var app = express();

var job = new CronJob({
  cronTime: '00 * * * * *', 
  onTick: function(){
    if(toProcess.size < 3000){
      co(function*(){
        var newchunks = yield db.urls.find({'meta.state': {$in: ['new', 'inprogress']}, $or: [{'meta.tries': {$lte: 10}}, {'meta.tries': {$exists: 0}}] }).limit(10000).toArray();
        var idsToUpdate = [];
        while(newchunks.length){
          var c = newchunks.pop();
          c._id = c._id.toString();
          if(!toProcess.has(c._id) && !inProcess.has(c._id)){
            toProcess.set(c._id, c);
            idsToUpdate.push(pmongo.ObjectId(c._id));
          }
        }
        var now = new Date();
        if(idsToUpdate.length){
          yield db.urls.update({_id: {$in: idsToUpdate}}, 
            {'$set':{
              'meta.state': 'inprogress',
              'meta.started': now,
              },
            }, 
            {
              multi: true
            }
          );
          console.log('adding new chunks: ' + idsToUpdate.length);
        }
   
      })
      .catch(console.error);
    }
  },
  runOnInit: true
}).start();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
//app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
