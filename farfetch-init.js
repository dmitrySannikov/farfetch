var cheerio = require('cheerio'),
  rp = require('request-promise'),
  co = require('co'),
  pmongo = require('promised-mongo'),
  zlib = require('zlib');

var db = pmongo('mongodb://localhost:27017/farfetch', ['urls', 'items']);

co(function*(){
  
  yield db.urls.drop();
  yield db.items.drop();
  
  console.log('Getting main sitemap...');
  var $ = yield rp({
    uri: 'https://www.farfetch.com/de/sitemap.xml',
    transform: function(body) {return cheerio.load(body)}
  });
  
  var lists = $('loc').map(function(){
    return $(this).text();
  }).get().filter(function(item){
    return item.indexOf('de') != -1 || item.indexOf('ru') != -1;
  })
  
  yield db.urls.insert(lists.map(function(u){
    return {
      meta:{
        url: u,
        type: 'list',
        state: 'new',
        transform: 'gzip'
      }
    }
  }));
  
  yield db.urls.createIndex({'meta.state': 1});
  yield db.urls.createIndex({'meta.started': 1});
  yield db.urls.createIndex({'meta.url': 1});
  yield db.urls.createIndex({'meta.type': 1});
  yield db.urls.createIndex({'meta.tries': 1});
  
  console.log('farfetch init DONE!');
})
.catch(console.error)  
.then(function(){
  db.close();
});