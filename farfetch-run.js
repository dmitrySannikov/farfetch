var cheerio = require('cheerio'),
  rp = require('request-promise'),
  r = require('request'),
  co = require('co'),
  pmongo = require('promised-mongo'),
  zlib = require('zlib'),
  parsers = {},
  process = require('process'),
  fs = require('fs');

  
var db = pmongo('mongodb://localhost:27017/farfetch', ['urls', 'items']);



var transforms = {
  gzip: function(body){
    return new Promise(function(resolve, reject){
       zlib.unzip(body, function(err, buffer){
         if(err) return reject(err);
         resolve(buffer.toString('utf8'));
       });
    })
  }
}

var getNextChunk = function(id){
  return rp({
    uri: 'http://127.0.0.1:3000/next' + (id ? ('?id='+id) : '') 
  })
}

function parseChunk(chunk){
  return co(function*(){
    if(chunk.meta){
      console.log(chunk.meta.type);
      if(!parsers[chunk.meta.type]){
        parsers[chunk.meta.type] = require('./farfetch/' + chunk.meta.type)
      }
      var parser = parsers[chunk.meta.type];
      if(parser){
        var transform = null;
        if(chunk.meta.transform){
          transform = transforms[chunk.meta.transform];
        }

        var error = false;
        console.log('loading ' + chunk.meta.url + '...');        
        var response = yield rp({
          uri: chunk.meta.url,
          resolveWithFullResponse: true,
          encoding: null
        })
        .catch(function(err){
          error = err;
        });
        if(error){
          yield db.urls.update({_id: pmongo.ObjectId(chunk._id)}, {$set: {
            'meta.state': 'error',
            'meta.lastError': {message: error.message, stack: error.stack}
          }});
        }
        else{
          var body = response.body;
          
          if(transform)
            body = yield transform(body);
          else
            body = body.toString('utf8');
            
          var links = parser(cheerio.load(body));
          console.log(links);
          if(links && links.length){
            var filteredLinks = [];
            for(var i=0;i<links.length;i++){
              var link=links[i];
              if (link.type === 'done' && link.name) {
              	link.url = chunk.meta.url;
                yield db.items.insert(link);
              }
                else {
                  if(link.filter && link.filter.unique){
                    var res = yield db.urls.find({'meta.url': link.url}).toArray();
                    if(!res || res.length == 0){
                      filteredLinks.push(link);
                    }
                  }
                  else{
                    filteredLinks.push(link);
                  }
                }
            }
            
            links = filteredLinks;
            
            var batchSize = 500;
            while(links.length > 0){
              var batch = [];
              for(var i=0;i<batchSize && links.length;i++){
                var l = {
                  meta: links.pop() 
                }; 
                l.meta.state = 'new';
                l.meta.parent = chunk._id;
                batch.push(l);
              }
              if(batch.length){
                if(batch.length == 1)
                  yield db.urls.insert(batch[0]);
                else
                  yield db.urls.insert(batch);
              }
            }
          }
          yield db.urls.update({_id: pmongo.ObjectId(chunk._id)}, {$set: {
            'meta.state': 'done',
            'meta.body': zlib.gzipSync(body),
            'meta.finalUrl': response.request.uri.href
          }});
        }
      }
    }      
  })
}

function run(nextId){
  var id = null;

  getNextChunk(nextId).then(function(chunk){
    chunk = JSON.parse(chunk);
    if(chunk && chunk.meta){
      id = chunk._id;
      return parseChunk(chunk);
    }
    else{
      db.close();
      process.exit(0);
    }

  })
  .then(function(){
    process.nextTick(function(){
      run(id);
    })
  })
  .catch(function(err){
    console.error(err);
    db.close();
    process.exit(0);
  })  
}
run(null);