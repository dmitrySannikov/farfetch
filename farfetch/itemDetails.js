var cheerio = require('cheerio');


module.exports = function($){

	var items = []

  var name = $('.detail-brand span').map(function(){
    return $(this).text().trim();
  }).get();

  var thingID = $('.designer-style-id span').map(function(){
  	return $(this).text().trim();
  }).get();

  var price = $('.pdp-price span.listing-price').map(function(){
  	return $(this).text().trim();
  }).get();

  var size = $('.accordion-item [data-tstid="Content_Size&Fit"] dd:nth-child(2)').map(function(){
  	return $(this).text().trim();
  }).get();

  var link = {
  	name: name,
  	thingID: thingID,
  	price: price,
  	size: size,
  	type: 'done'
  }
  
  items.push(link)

  return items;
}