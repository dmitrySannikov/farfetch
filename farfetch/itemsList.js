module.exports = function($){

  var links = $('article.baseline > a').map(function(){
    var url;
    if ($(this).attr('href').match(/https:\/\/www.farfetch.com\/.+/g)){
      url = $(this).attr('href')
    } else {
      url = 'https://www.farfetch.com' + $(this).attr('href');
    }
    return {
      url: url,
      type: 'itemDetails',
      filter:{
        unique: true
      }
    }
  }).get();
  
  return links; 
  
}
