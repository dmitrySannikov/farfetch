module.exports = function($){
  var links = $('loc').map(function(){
    return $(this).text();
  }).get().filter(function(l){
    return l.indexOf('/buy/') != -1 
      || l.indexOf('/shopping/') != -1
      || l.indexOf('/sets/') != -1
  });
  return links.map(function(l){
    return {
      url: l,
      type: 'itemsList',
      filter:{
        unique: true
      }
    };
  });
}
