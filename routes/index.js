'use strict'
var  express = require('express');
var router = express.Router(), 
    chunksInProcess = null, 
    chunksToProcess = null, 
    moment = require('moment'), 
    pmongo = require('promised-mongo');



router.get('/next', function(req, res, next){
  var now = moment();
  
  var toProcessKeys = chunksInProcess.keys();
  for(var i=0;i<toProcessKeys.length;i++){
    var c = chunksInProcess.get(toProcessKeys[i]);
    if(now.diff(c.meta.started, 'minutes', true) > 5){
      chunksInProcess.delete(c._id);
      chunksToProcess.set(c._id, c);
    }
  }

  if(req.query.id){
    chunksInProcess.delete(req.query.id);
  }
  
  if(chunksToProcess.size){
    var mapIter = chunksToProcess[Symbol.iterator]();
    var c = mapIter.next().value[1];
    chunksToProcess.delete(c._id);
    chunksInProcess.set(c._id, c);
    res.json(c);
  }
  else{
    res.json({});
  }
})

router.get('/inprocess', function(req, res, next){
  
  var chunks = []
  chunksInProcess.forEach(function(value) {
    chunks.push(value);
  });
  res.json(chunks);
})

module.exports = function(toProcess, inProcess){
  chunksInProcess = inProcess;
  chunksToProcess = toProcess;
  return router;
};
